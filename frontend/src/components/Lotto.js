import {Component} from "react";
import Kegs from "./Kegs";

class Lotto extends Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.state = {
            now: false
        };
        this.numbers = [];
    }

    handleClick() {
        this.numbers = [];
        for (let i = 1; i <= 5; i++) {
            let numeric = this.getRandomArbitrary(5 , 36)
            if (this.numbers.some(o => o.num === numeric)){
                i--;
            }else{
                this.numbers.push({ num: numeric})
            }

        }

        this.setState({now: true})
    }

    getRandomArbitrary(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    }
    playLotto(){
        this.numbers.sort((a, b) => a.num > b.num ? 1 : -1);
    }

    render() {
        this.playLotto();
        const now = this.state.now
        let sms ;
        if (now) {
            sms = ( this.numbers.map((key)=>(
                    <Kegs key={key.num} number={key.num}/>
                )))

        }
        return (<div>
                <button onClick={this.handleClick}>Играть</button>
                <div className="main">
                    {sms}
                </div>
            </div>
        )

    }
}
export default Lotto ;
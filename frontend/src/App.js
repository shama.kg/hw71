import './App.css';
import Lotto from "./components/Lotto";
import React from "react";

class App extends React.Component{
    render() {
        return (
            <div className="App">
                <header className="App-header">

                    <Lotto/>

                </header>
            </div>
        );
    }

}

export default App;
